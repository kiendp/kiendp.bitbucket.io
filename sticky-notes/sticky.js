Element.prototype.css = function (style) {
   for (prop in style) {
      if (style.hasOwnProperty(prop))
         this.style.setProperty(prop, style[prop]);
   }
}

window.addEventListener('load', () => {
   const notes = { ...localStorage
   };
   for (const id in notes) {
      viewNote(id, notes[id]);
   }
}, false);

document.getElementById('takeNote').addEventListener('click', () => {
   document.getElementsByClassName('modal-note')[0].classList.add('show')
}, false);

document.getElementById('close').addEventListener('click', closeNote, false);

document.getElementById('stickyNote').addEventListener('click', () => {
   const content = document.getElementsByClassName('paper-content')[0].value.trim();
   if (content === '') {
      alert('You must enter something to save');
   } else if (content.length > 230) {
      alert('Hmmm! It seem that your note is too long :/');
   } else {
      let note = Object.create(Note);
      note.createNote(content);
      closeNote();
   }
}, false);

let Note = {
   id: null,
   content: null,
   setNote: function (content) {
      if (localStorage) {
         this.id = (+new Date * Math.random()).toString(36).substr(0, 5);
         this.content = content;
         localStorage.setItem(this.id, this.content);
         return this.id;
      } else {
         alert('Sorry! Your browser does not support web storage');
      }
   },
   getNote: function (id) {
      localStorage.getItem(id);
   },
   delNote: function (id) {
      localStorage.removeItem(id);
   },
   createNote: function (content) {
      let regexLink = /a\[(.+)\](.*)\./gm;
      content = content.replace(regexLink, '<a href="$1">$2</a>');
      const id = this.setNote(content);
      if (!!id) viewNote(id, content);
      else alert('Sorry, something went wrong when created your note');
   }
};

function closeNote() {
   document.getElementsByClassName('paper-content')[0].value = '';
   document.getElementsByClassName('modal-note')[0].classList.remove('show');
}

function viewNote(id, content) {
   let newNote = document.getElementById('note-sample').cloneNode(true);
   const pushpin = ['#18e634', '#24b8da', '#cb3433'];

   newNote.removeAttribute('id');
   newNote.getElementsByClassName('noteId')[0].value = id;
   newNote.getElementsByClassName('note-body')[0].innerHTML = content;
   newNote.getElementsByClassName('pushpin')[0].style.setProperty('background', pushpin[rand(1, 3)]);
   newNote.getElementsByClassName('pushpin')[0].addEventListener('click', function () {
      const note = Object.create(Note), htmlNote = this.closest('.note');
      note.delNote(id);

      document.body.style.setProperty('overflow', 'hidden');
      
      htmlNote.animate([
         {transform: 'scaleY(-1)'},
         {transform: `translateY(${innerHeight - this.getBoundingClientRect().bottom}px)`}
         ], {
            duration: 2000,
            fill: 'forwards'
         });

      setTimeout(() => {
         htmlNote.remove();
         document.body.style.setProperty('overflow', 'auto');
      }, 2000);

   }, false);
   document.getElementsByClassName('row')[0].appendChild(newNote);
}

function rand(min, max) {
   min = Math.ceil(min);
   max = Math.floor(max);
   return Math.floor(Math.random() * (max - min + 1)) + min;
}